const express = require('express')
const { graphqlExpress, graphiqlExpress } = require('graphql-server-express')
const bodyParser = require('body-parser')
const cors = require('cors')

const { executableSchema } = require('./schema')

const APP_PORT = 3000

let app = express()

app.use('/graphql', cors(), bodyParser.json(), graphqlExpress((req) => {
  return {
    schema: executableSchema,
    context: {
      user: req.user
    }
  }
}))

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql'
}))

app.listen(APP_PORT, () => {
  console.log(`App is now running on http://localhost:${APP_PORT}`)
})
