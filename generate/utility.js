function camelize (str, fullCase = false) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
    return index === 0 && !fullCase ? letter.toLowerCase() : letter.toUpperCase()
  }).replace(/\s+/g, '')
}

function updateObjectInArray (array, action) {
  const [ key, value ] = action.find
  return array.map((item) => {
    if (item[key] !== value) {
      // This isn't the item we care about - keep it as-is
      return item
    }

    // Otherwise, this is the one we want - return an updated value
    return {
      ...item,
      [ action.updateKey ]: [ ...item[action.updateKey], ...action.update ]
    }
  })
}

module.exports.camelize = camelize
module.exports.updateObjectInArray = updateObjectInArray
