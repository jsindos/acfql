/* global expect, describe, it */

const ACFStore = require('../../generate/acfstore')
const { updateObjectInArray, camelize } = require('../../generate/utility')

describe('generate resolvers file', () => {
  describe('utility functions', () => {
    it('updateObjectInArray', () => {
      const fieldGroups = [ { foo: 'bar' }, {
        fullName: 'Apple Information',
        fields: [
          {
            fullName: 'price',
            connectorName: 'getAppleInformationPrice'
          }
        ]
      } ]
      const update = updateObjectInArray(fieldGroups, {
        find: [ 'fullName', 'Apple Information' ],
        update: [ {
          fullName: 'cultivar',
          connectorName: camelize(`get Apple Information cultivar`)
        } ],
        updateKey: 'fields'
      })
      expect(update.find(u => u.fullName === 'Apple Information').fields).toHaveLength(2)
      expect(update).toMatchSnapshot()
    })

    it('updateObjectInArray initial', () => {
      // `fields` needs to be initialised as empty array
      const fieldGroups = [ { fullName: 'Apple Information', fields: [] } ]
      const update = updateObjectInArray(fieldGroups, {
        find: [ 'fullName', 'Apple Information' ],
        update: [ {
          fullName: 'cultivar',
          connectorName: camelize(`get Apple Information cultivar`)
        } ],
        updateKey: 'fields'
      })
      expect(update.find(u => u.fullName === 'Apple Information').fields).toHaveLength(1)
    })
  })

  describe('ACFStore', () => {
    it('addFieldGroupRelation', () => {
      const a = new ACFStore()
      a.addCustomPostType('wp_apple')
      a.addFieldGroupRelation('wp_apple', 'Apple Information')
      expect(a.customPostTypes.find(c => c.fullName === 'wp_apple').fieldGroups).toHaveLength(1)
    })
  })
})
