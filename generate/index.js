const ACFStore = require('./acfstore')
const mkdirp = require('mkdirp')
const fs = require('fs')
const getDirName = require('path').dirname
const path = require('path')

require.context = require('./requireContextPolyfill')

const pathToLocal = path.join(__dirname, '..')
const templates = require.context(pathToLocal, true, /^(?!.*node_modules).*\.template\.js$/)

// Read JSON file
// https://stackoverflow.com/questions/10011011/using-node-js-how-do-i-read-a-json-object-into-server-memory
const store = new ACFStore()
const json = JSON.parse(fs.readFileSync('./apple-information.json', 'utf8'))
json.forEach(fieldGroup => {
  store.addFieldGroup(fieldGroup.title)
  fieldGroup.fields.forEach(field => {
    store.addField(field.name, fieldGroup.title, field.type)
  })
  fieldGroup.location.forEach(location => {
    location.forEach(l => {
      store.addCustomPostType(l.value)
      store.addFieldGroupRelation(l.value, fieldGroup.title)
    })
  })
})

// store.addCustomPostType('wp_apple')
// store.addFieldGroupRelation('wp_apple', 'Apple Information')
// store.addFieldGroup('Apple Information')
// store.addField('cultivar', 'Apple Information', 'text')

function writeFile(path, contents, cb) {
  // https://stackoverflow.com/questions/16316330/how-to-write-file-if-parent-folder-doesnt-exist
  mkdirp(getDirName(path), function (err) {
    if (err) return cb(err)
    fs.writeFile(path, contents, cb)
  })
}

Object.entries(templates).forEach(([ templatePath, { generateTemplate } ]) => {
  const template = generateTemplate(store.customPostTypes, store.fieldGroups)
  if (Array.isArray(template)) {
    template.forEach(t => {
      const fileName = path.join(path.dirname(templatePath), `${t.fileName}.js`).replace(/src/, 'lib')
      writeFile(fileName, t.template, (err) => err && console.log(err))
    })
  }
  else {
    const fileName = templatePath.replace(/.template/, '').replace(/src/, 'lib')
    writeFile(fileName, template, (err) => err && console.log(err))
  }
})

console.log('Your graphql schema has been generated.')

module.exports.writeFile = writeFile
