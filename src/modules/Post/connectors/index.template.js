module.exports.generateTemplate = () =>
`// import getPost from './getPost'
const getPosts = require('./getPosts')
// import getPostTerms from './getPostTerms'
// import getTermPosts from './getTermPosts'
// import getPostLayout from './getPostLayout'

module.exports = function ({ Post, Postmeta, Terms, TermRelationships }) {
  return {
    // getPost: getPost(Post),
    getPosts: getPosts(Post),
    // getPostTerms: getPostTerms(Terms, TermRelationships, settings),
    // getTermPosts: getTermPosts(TermRelationships, Post, settings),
    // getPostLayout: getPostLayout(Postmeta)
  }
}
`
