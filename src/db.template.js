function generateTemplate (customPostTypes, fieldGroups) {
  return `const Sequelize = require('sequelize')

const PostModel = require('./modules/Post/model')
const PostmetaModel = require('./modules/Postmeta/model')

const PostConnectors = require('./modules/Post/connectors')
const PostmetaConnectors = require('./modules/Postmeta/connectors')
const CustomFieldsConnectors = require('./modules/CustomFields/connectors')

class Database {
  constructor (settings) {
    this.settings = settings
    this.connection = this.connect(settings)
    this.connectors = this.getConnectors()
    this.models = this.getModels()
  }

  connect () {
    const { name, username, password, host, port } = this.settings.privateSettings.database

    const Conn = new Sequelize(
      name,
      username,
      password,
      {
        logging: false,
        dialect: 'mysql',
        host: host,
        port: port || 3306,
        define: {
          timestamps: false,
          freezeTableName: true
        }
      }
    )

    return Conn
  }

  getModels () {
    const prefix = this.settings.privateSettings.wp_prefix
    const Conn = this.connection

    return {
      Post: PostModel(Conn, prefix),
      Postmeta: PostmetaModel(Conn, prefix)
    }
  }

  getConnectors () {
    const models = this.getModels()

    return {
      ...PostConnectors(models),
      ...PostmetaConnectors(models),
      ...CustomFieldsConnectors(models)
    }
  }
}

module.exports = Database
`
}

module.exports.generateTemplate = generateTemplate
