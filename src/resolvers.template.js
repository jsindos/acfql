function generateTemplate (customPostTypes, fieldGroups) {
  return `function Resolvers (Connectors) {
  const Resolvers = {
    Query: {
      ${customPostTypes.map(c =>
      `
      ${c.pluralizedName} () {
        return Connectors.getPosts({ post_type: '${c.fullName}' })
      },
      `
      ).join('')}
    },
    ${customPostTypes.map(c =>
    `
    ${c.fullCaseName}: {
      ${c.fieldGroups.map(f =>
      `
      ${f} (post) {
        return post
      },
      `
      ).join('')}
    },
    `
    ).join('')}
    ${fieldGroups.map(g =>
    `
    ${g.fullCaseName}: {
      ${g.fields.map(f =>
      `
      ${f.fullName} (post) {
        return Connectors.${f.connectorName}({ postId: post.id })
      },
      `
      ).join('')}
    },
    `
    ).join('')
    }
  }

  return Resolvers
}

module.exports = Resolvers
`
}

module.exports.generateTemplate = generateTemplate
