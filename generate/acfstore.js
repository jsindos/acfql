const pluralize = require('pluralize')

const { updateObjectInArray, camelize } = require('./utility')

class ACFStore {
  constructor () {
    this.customPostTypes = []
    this.fieldGroups = []
  }

  addCustomPostType (customPostType) {
    this.customPostTypes = this.customPostTypes.find(c => c.fullName === customPostType)
      ? this.customPostTypes
      : this.customPostTypes.concat([ {
        fullName: customPostType,
        pluralizedName: pluralize.plural(customPostType.replace(/^wp_/, '')),
        fullCaseName: camelize(customPostType.replace(/^wp_/, ''), true),
        fieldGroups: []
      } ])
  }

  addFieldGroupRelation (customPostType, fieldGroupName) {
    this.customPostTypes = updateObjectInArray(this.customPostTypes, {
      find: [ 'fullName', customPostType ],
      update: [ camelize(fieldGroupName) ],
      updateKey: 'fieldGroups'
    })
  }

  addFieldGroup (fieldGroupName) {
    this.fieldGroups = this.fieldGroups.find(f => f.fullName === fieldGroupName)
      ? this.fieldGroups
      : this.fieldGroups.concat([ {
        fullCaseName: camelize(fieldGroupName, true),
        fullName: fieldGroupName,
        fields: []
      } ])
  }

  addField (fieldName, fieldGroupName, type = 'text') {
    this.fieldGroups = updateObjectInArray(this.fieldGroups, {
      find: [ 'fullName', fieldGroupName ],
      update: [ {
        fullName: fieldName,
        connectorName: camelize(`get ${fieldGroupName} ${fieldName}`),
        type
      } ],
      updateKey: 'fields'
    })
  }
}

module.exports = ACFStore
