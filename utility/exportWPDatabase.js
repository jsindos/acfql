const Sequelize = require('sequelize')
const http = require('http')
const fs = require('fs')

const { writeFile } = require('../generate/index')

const { privateSettings, publicSettings } = require('../settings')

class WordExpressDatabase {
  constructor (settings) {
    this.settings = settings
    this.connection = this.connect(settings)
  }

  connect () {
    const { name, username, password, host, port } = this.settings.privateSettings.database

    const Conn = new Sequelize(
      name,
      username,
      password,
      {
        logging: false,
        dialect: 'mysql',
        host: host,
        port: port || 3306,
        define: {
          timestamps: false,
          freezeTableName: true
        }
      }
    )

    return Conn
  }

  getModel () {
    const prefix = this.settings.privateSettings.wp_prefix
    const Conn = this.connection

    return Conn.define(prefix + 'postmeta', {
      meta_id: { type: Sequelize.INTEGER, primaryKey: true, field: 'meta_id' },
      post_id: { type: Sequelize.INTEGER },
      meta_key: { type: Sequelize.STRING },
      meta_value: { type: Sequelize.INTEGER },
    })
  }
}

const Database = new WordExpressDatabase({ privateSettings, publicSettings })
const Model = Database.getModel()
Database.connection.sync()
  .then(() => Model.findAll())
  .then(values => exportData(values))

const exportData = (values) => {
  console.log(values.map(v => v.dataValues))
  const contents = `module.exports = [
${values.map(v => JSON.stringify(v.dataValues)).join(',\n')}
]
`
  writeFile('./export/export.js', contents, err => err && console.log(err))
}

const downloadAttachments = (attachments) => {
  // https://stackoverflow.com/questions/11944932/how-to-download-a-file-with-node-js-without-using-third-party-libraries
  if (!fs.existsSync('./media')) {
    fs.mkdirSync('./media')
  }
  attachments.forEach(a => {
    const fileName = a.guid.replace(/^.*[\\/]/, '')
    const file = fs.createWriteStream(`./media/${fileName}`)
    http.get(a.guid, function (response) {
      console.log(`Downloaded ${a.guid}`)
      response.pipe(file)
    }).on('error', function () { // Handle errors
      fs.unlink(file) // Delete the file async. (But we don't check the result)
    })
  })
}
