function generateTemplate (customPostTypes, fieldGroups) {
  return `${customPostTypes.map(c =>
`const ${c.fullCaseName} = require('./${c.fullCaseName}')`
)}

const RootQuery = \`
  scalar JSON

  type Query {
    ${customPostTypes.map(c =>
    `${c.pluralizedName}: [${c.fullCaseName}]`
    )}
  }
\`

const SchemaDefinition = \`
  schema {
    query: Query
  }
\`

module.exports = [
  ${customPostTypes.map(c =>
  `${c.fullCaseName},`
  )}
  RootQuery,
  SchemaDefinition
]
`
}

module.exports.generateTemplate = generateTemplate
