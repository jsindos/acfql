### acfql introduction

GraphQL is a new query language and transfer protocol developed by Facebook. It is an alternative to REST, where GraphQL queries are more explicitly defined and can have a greater nested structure. It is independent of the backend technology.

The purpose of this project is to enable fast development of a decoupled frontend and backend using advanced custom fields on WordPress. We have decided to explore the potential of GraphQL as it lends itself well to React `props` and the composite nature of React.

### Setup

1. Install fresh WordPress
2. Install [ACF PRO](https://www.advancedcustomfields.com/pro/), your desired [custom post types](https://codex.wordpress.org/Custom_Post_Types) and [advanced-custom-fields-wpcli](https://github.com/hoppinger/advanced-custom-fields-wpcli) plugins
3. Set up your advanced custom fields and attach them to your custom post types
4. Dump your advanced custom fields and custom post types to JSON using the following command from advanced-custom-fields-wpcli: `wp acf export --export_path=acf-exports/`. Note you must correctly [set up](https://wp-cli.org/#installing) wp-cli in order to be able to use this command.
5. Copy the exported JSON file to the root of this project and rename it `apple-information.json` (this is hardcoded for the moment and will be changed in an upcoming release this week).
6. `cd` into `acfql`, run `npm install` and then run `npm run build`. This will generate the GraphQL schema of your advanced custom fields and custom post types as according to the exported JSON file.
7. Run `node server.js` or `nodemon server.js` to serve up your new schema.
8. Navigate to [http://localhost:3000/graphiql](http://localhost:3000/graphiql) to experience your new advanced custom fields GraphQL schema!